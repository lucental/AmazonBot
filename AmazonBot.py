import discord
from discord.ext import commands
import asyncio
import re

client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

@client.event
async def on_message(message):
	if message.author.id != "BOT USER ID HERE":
		if "https://www.amazon.com" in message.content.lower():
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.com/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
		if "https://www.amazon.ca" in message.content.lower():
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.ca/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
		if "https://www.amazon.co.uk" in message.content.lower():
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.co.uk/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
		if "https://www.amazon.de" in message.content.lower():
			await client.send_message(message.channel, "Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.")
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.de/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
		if "https://www.amazon.es" in message.content.lower():
			await client.send_message(message.channel, "Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.")
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.es/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
		if "https://www.amazon.fr" in message.content.lower():
			await client.send_message(message.channel, "Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.")
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.fr/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
		if "https://www.amazon.it" in message.content.lower():
			await client.send_message(message.channel, "Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.")
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.it/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
		if "https://www.amazon.jp" in message.content.lower():
			await client.send_message(message.channel, "Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.")
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.jp/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
		if "https://www.amazon.cn" in message.content.lower():
			await client.send_message(message.channel, "Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.")
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.cn/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
		if "https://www.amazon.in" in message.content.lower():
			await client.send_message(message.channel, "Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.")
			regex = re.compile('B[A-Z0-9]{9}')
			result = regex.search(message.content)

			embed = discord.Embed(title="Amazon link converted!", description="Ordering through this referral link gives us a small kickback from your order while adding nothing to your overall cost.", color=0x9400D3)
			embed.set_thumbnail(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThGU9r2Vi7WLYoTQGuT48UeU0p6hfhsiP75Ubaz0mg3xzgC185')
			embed.add_field(name="Affiliate link:", value="https://www.amazon.in/dp/" + result.group(0) + "/?tag=TAG HERE", inline=False)
			await client.send_message(message.channel, embed=embed)
			
client.run("TOKEN HERE", bot=True)