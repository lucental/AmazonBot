# What is AmazonBot?
AmazonBot is a simple Discord bot that automatically reads any messages consisting of Amazon links and returns the link with the owner's referral tag added on top. This way you can easily encourage members of your Discord server to support you by generating revenue through the Amazon Affiliate program easily through an automated process. The bot works with all conditions: when the link is its own message, or when the link is at the start, middle, or end of a message. 

# What does it look like?
![](https://i.imgur.com/UoyhVpb.png)

All parts of the output can be easily modified, including all of the text, the thumbnail image, and how the embed is handled. With basic knowledge of Python and the Discord API, you can also easily add your own custom commands to the bot.

# Setup
1. Make sure you have the most recent version of Python installed on your computer before you proceed.
2. Download the repository as a .zip file by clicking the cloud icon in the top-right corner.
3. Extract the .zip file and open the contents of AmazonBot.py with your editor of choice. I recommend against using Windows Notepad: instead, use something like Notepad++ or Atom.
4. Use the Find function (Ctrl + F) to highlight all of instances of the term "TAG HERE". Right after "/?tag=", replace the text with your Amazon affiliate tag for each regional variant. Don't have an Affiliate account for all of the regional variants of Amazon? Remove or comment out the blocks of code for the ones you don't have.
5. Create a bot user on Discord using this guide: https://discordpy.readthedocs.io/en/rewrite/discord.html
6. Copy the token of the bot user you just created. At the final line of the code, replace "TOKENHERE" with your bot user's token. 
7. Save your changes and run AmazonBot.py. If you recieve confirmation that the bot is logged in, congrats! Your new AmazonBot is functioning!
8. Invite your AmazonBot to your Discord server. Go to the following link but replace "YOUR_CLIENT_ID" with the Client ID listed on your bot user's page: https://discordapp.com/oauth2/authorize?client_id=YOUR_CLIENT_ID&scope=bot&permissions=0

# Issues
Do note that at this point in time the bot does not have support for Amazon Brazil, Amazon Australia and Amazon Mexico. I am working to fix the problems surrounding this and they should be added to the bot soon.

# Credits
Special thanks to Jellocan for helping me with Discord embed, making the output look a lot cleaner and nicer. All of the other code was created by me.

# License
This bot is licensed under the GNU GPLv3 open-source license. To learn more about the GNU GPLv3, read the full licensing terms here: https://www.gnu.org/licenses/gpl-3.0.en.html

# Contribute
Appreciate the work I've done on AmazonBot and want to support me for the other software I make in the future? [Feel free to throw a few bucks my way on my Ko-fi page!](https://www.ko-fi.com/lucental) This is absolutely not required in any way, but hey, from all the extra Amazon Affilate revenue you'll probably be getting from this, maybe you can put a bit of that towards supporting me? :p